const express = require('express')
const mongoose = require('mongoose')
const url = 'mongodb://localhost/Faizan'

const app = express()

mongoose.connect(url);

const con = mongoose.connection;

con.on('open' ,() => {
    console.log("Connected......");
})

const aliensRouter = require('./routes/aliens')
app.use(express.json())


app.use('/aliens',aliensRouter)

app.use(express.json())

app.listen(9000,() =>{
    console.log("Server Started");
})